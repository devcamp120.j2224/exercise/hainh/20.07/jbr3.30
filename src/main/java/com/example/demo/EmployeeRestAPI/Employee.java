package com.example.demo.EmployeeRestAPI;

public class Employee {
    private int id ;
    private String firstname ;
    private String lastname ;
    private int salary ;
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName(String firstname , String lastname){
        return this.firstname + " " + this.lastname ;
    }
    public Employee() {

    }

    public Employee(int id, String firstname, String lastname, int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.salary = salary;
    }

    public int getAnualSalary(int salary){
        return salary * 12 ;
    }

    public int raiseSalary(int percent) {
        return ((percent/100)+1) * this.salary ;
    }

    @Override
    public String toString() {
        return "Employee : " + this.id + " , fullname : " + getName(firstname, lastname) + ", salary : " + this.salary + " , salary 1 year : " + getAnualSalary(this.salary) + " , salary : " + this.salary ;
    }

    



    
}
